// -*- coding:utf-8; tab-width:4; mode:cpp -*-
// Minesweeper author: Isaac Lacoba Molina
//   Copyright (C) 2014  ISAAC LACOBA MOLINA
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef INITIAL_H
#define INITIAL_H
#include "state.h"


class Initial: public State {
 public:
  typedef std::shared_ptr<Initial> shared;

  Initial(std::shared_ptr<Game> game);

  virtual void init();
  virtual void resume();
  virtual void exit();

  virtual void update();

  ~Initial();

 private:
  void add_callbacks();
  void init_initial();
  void create_menu();
  void create_difficulty_menu();
  void create_board();
};

#endif
