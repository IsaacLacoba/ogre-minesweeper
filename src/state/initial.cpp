// -*- coding:utf-8; tab-width:4; mode:cpp -*-
// Minesweeper author: Isaac Lacoba Molina
//   Copyright (C) 2014  ISAAC LACOBA MOLINA
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "game.h"


Initial::Initial(Game::shared game): State(game) {
}

Initial::~Initial() {

}

void
Initial::init() {
  init_initial();
  create_menu();
  create_difficulty_menu();
  create_board();
}

void
Initial::resume() {

}

void
Initial::exit() {
  State::game_->input_->clear_hooks();
  State::game_->scene_->remove_child("", "initial_menu");
  State::game_->set_current_state("menu");
}

void
Initial::update() {

}

void
Initial::add_callbacks() {
  State::game_->input_->add_hook({OIS::MB_Left}, std::bind(&Initial::exit, this));
}

void
Initial::init_initial() {
  State::game_->scene_->create_node("initial_menu");
  State::create_graphic_element("initial_menu", "initial_title", "title.mesh", "titulo",
                                Ogre::Vector3(12, 0, 2), Ogre::Vector3(0, 1, 0), true);
  State::create_graphic_element("initial_menu", "initial_letter", "initial_letter.mesh",
                                "initial_letter", Ogre::Vector3(6, 0, 0.4),
                                Ogre::Vector3(0, -2, 0), true);
  State::game_->scene_->add_child("", "initial_menu");
  add_callbacks();
}

void
Initial::create_menu() {
  State::game_->scene_->create_node("menu_root");
  State::create_graphic_element("menu_root", "titlenode", "letter.mesh", "titulo",
                                Ogre::Vector3(12, 0, 2), Ogre::Vector3(0, 5, 0));
  State::create_graphic_element("menu_root", "playbtn_node", "playbtn.mesh", "play_btn",
                                Ogre::Vector3(3.5, 0, 1), Ogre::Vector3(0, 0, -1));
  State::create_graphic_element("menu_root", "creditsbtn_node", "creditsbtn.mesh", "credits_btn",
                                Ogre::Vector3(3.5, 0, 1), Ogre::Vector3(0, -2.5, -1));
  State::create_graphic_element("", "credits_node", "credits.mesh", "credits",
                                Ogre::Vector3(9, 0, 8), Ogre::Vector3(0, -2, -1));
  State::game_->scene_->remove_child("", "credits_node");
  State::create_graphic_element("menu_root", "exitbtn_node",  "exitbtn.mesh", "exit_btn",
                                Ogre::Vector3(3.5, 0, 1), Ogre::Vector3(0, -5, -1));
}

void
Initial::create_difficulty_menu() {
  State::game_->scene_->create_node("difficulty_root");
  State::create_graphic_element("difficulty_root", "difficultytitle_node",
                                "difficulty_title.mesh", "difficulty_title",
                                Ogre::Vector3(10, 0, 1), Ogre::Vector3(0, 5, 0));
  State::create_graphic_element("difficulty_root", "easy_node", "easy.mesh", "easy",
                                Ogre::Vector3(4, 0, 0.7), Ogre::Vector3(0, 1, 0));
  State::create_graphic_element("difficulty_root", "medium_node", "medium.mesh", "medium",
                                Ogre::Vector3(4, 0, 0.7), Ogre::Vector3(0, -1, 0));
  State::create_graphic_element("difficulty_root", "hard_node", "hard.mesh", "hard",
                                Ogre::Vector3(4, 0, 0.7), Ogre::Vector3(0, -3, 0));
}

void
Initial::create_board() {
  State::game_->scene_->create_node("board_root");

  State::create_graphic_element("", "win_node", "win.mesh", "win",
                                Ogre::Vector3(5, 0, 5), Ogre::Vector3(0, 0, 0));
  State::game_->scene_->get_node("win_node")->pitch(Ogre::Degree(60));
  State::game_->scene_->remove_child("", "win_node");

  State::create_graphic_element("", "lose_node", "lose.mesh", "lose",
                                Ogre::Vector3(5, 0, 5), Ogre::Vector3(0, 0, 0));
  State::game_->scene_->get_node("lose_node")->pitch(Ogre::Degree(60));
  State::game_->scene_->remove_child("", "lose_node");

}
