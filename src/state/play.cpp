// -*- coding:utf-8; tab-width:4; mode:cpp -*-
// Minesweeper author: Isaac Lacoba Molina
//   Copyright (C) 2014  ISAAC LACOBA MOLINA
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "game.h"

Play::Play(Game::shared game): State(game) {
}

Play::~Play() {
}

void
Play::init() {
  finish_ = false;
  State::game_->scene_->add_child("", "difficulty_root");

  add_callbacks();
}

void
Play::exit() {
  destroy_board();
  State::game_->input_->clear_hooks();
  State::button_triggers_.clear();

  if(win_)
    State::game_->scene_->remove_child("", "win_node");
  else
    State::game_->scene_->remove_child("", "lose_node");

  State::game_->set_current_state("menu");
}

void
Play::add_callbacks() {
  State::game_->input_->add_hook({OIS::MB_Left}, std::bind(&Play::click_button, this));
  State::game_->input_->add_hook({OIS::MB_Right}, std::bind(&Play::put_flag, this));

  State::add_hook("easy_node", std::bind(&Play::init_board, this, Minesweeper::Difficulty::Easy));
  State::add_hook("medium_node", std::bind(&Play::init_board, this, Minesweeper::Difficulty::Medium));
  State::add_hook("hard_node", std::bind(&Play::init_board, this, Minesweeper::Difficulty::Hard));
}

void
Play::register_cells_callbacks() {
  for(auto cell: minesweeper_.board_) {
    State::add_hook(cell.node_->getName(), std::bind(&Play::reveal_cell, this));
  }
  State::add_hook("win_node", std::bind(&Play::exit,this));
  State::add_hook("lose_node", std::bind(&Play::exit,this));
}

void
Play::init_board(Minesweeper::Difficulty level) {
  State::game_->scene_->remove_child("", "difficulty_root");
  move_camera(level);

  minesweeper_ = Minesweeper();
  init_ground();
  minesweeper_.initialize_board(level);


  init_cells_node_and_entity();
  register_cells_callbacks();
  State::game_->scene_->add_child("", "board_root");

}

void
Play::init_ground() {
  State::create_graphic_element("board_root",
                                "ground", "ground", "ground",
                                Ogre::Vector3(80, 0, 80),
                                Ogre::Vector3(5, -10, -15),
                                false);
}

void
Play::init_cells_node_and_entity() {
  Ogre::Vector3 position, scale = Ogre::Vector3(1, 1, 1);
  std::pair<Ogre::SceneNode*, Ogre::Entity*> graphic_cell;
  std::stringstream name;

  for(Cell& cell: minesweeper_.board_) {
    position = Ogre::Vector3(cell_offset * cell.get_position().first, 0,
                             cell_offset * cell.get_position().second);
    name.str("");
    name << "cell_node" << "x" << cell.get_position().first
         << "y" << cell.get_position().second;
    graphic_cell = State::create_graphic_element("board_root",name.str(), name.str(),
                                                 "cell-material", scale, position, false);

    name.str("");
    name << "flag" << "x" << cell.get_position().first
         << "y" << cell.get_position().second;
    State::create_graphic_element(graphic_cell.first->getName(),
                                  name.str(), name.str(), "",
                                  Ogre::Vector3(0.5, 0.5, 0.5),
                                  Ogre::Vector3(0, 2, 0),
                                  true, "flag.mesh");
    cell.initialize(graphic_cell.first, graphic_cell.second);
  }
}

void
Play::reveal_cell() {

  if(finish_)
    return;

  int cell = get_clicked_button_node_index();
  minesweeper_.active_cell(cell);

  if(minesweeper_.is_game_lose()) {
    State::game_->scene_->add_child("", "lose_node");
    win_ = false;
    finish_ = true;
    return;
  }
  if(minesweeper_.is_game_win()) {
    State::game_->scene_->add_child("", "win_node");
    win_ = true;
    finish_ = true;
    return;
  }
}

void
Play::put_flag() {
  if(finish_)
    return;

  State::get_clicked_node();
  int index = get_clicked_button_node_index();
  if(index != -1)
    minesweeper_.put_flag(index);
}

void
Play::resume() {

}

void
Play::update() {

}

int
Play::get_clicked_button_node_index() {

  if(State::clicked_button_ == "none")
    return -1;

  Ogre::SceneNode* clicked_button_node = State::game_->
    scene_->get_node(State::clicked_button_);
  return minesweeper_.get_node_index(clicked_button_node, cell_offset);
}

void
Play::move_camera(Minesweeper::Difficulty level) {
  if(level == Minesweeper::Difficulty::Easy)
    move_camera_easy();
  if(level == Minesweeper::Difficulty::Medium)
    move_camera_medium();
  if(level == Minesweeper::Difficulty::Hard)
    move_camera_hard();
}

void
Play::move_camera_easy() {
  State::game_->scene_->camera_->setPosition(Ogre::Vector3(0, 20, 20));
  State::game_->scene_->camera_->lookAt(Ogre::Vector3(0, 0, 0));
  State::game_->scene_->camera_->move(Ogre::Vector3(5, 0, 5));
  State::game_->scene_->get_node("win_node")->setPosition(Ogre::Vector3(7, 7, 12));
  State::game_->scene_->get_node("lose_node")->setPosition(Ogre::Vector3(7, 7, 12));
}

void
Play::move_camera_medium() {
  State::game_->scene_->camera_->setPosition(Ogre::Vector3(0, 30, 20));
  State::game_->scene_->camera_->lookAt(Ogre::Vector3(0, 0, 0));
  State::game_->scene_->camera_->move(Ogre::Vector3(14, 0, 20));
  State::game_->scene_->get_node("win_node")->setPosition(Ogre::Vector3(14, 15, 28));
  State::game_->scene_->get_node("lose_node")->setPosition(Ogre::Vector3(14, 15, 28));
}

void
Play::move_camera_hard() {
  State::game_->scene_->camera_->setPosition(Ogre::Vector3(0, 49, 30));
  State::game_->scene_->camera_->lookAt(Ogre::Vector3(0, 0, 10));
  State::game_->scene_->camera_->move(Ogre::Vector3(13, 0, 15));
  State::game_->scene_->get_node("win_node")->setPosition(Ogre::Vector3(15, 35, 40));
  State::game_->scene_->get_node("lose_node")->setPosition(Ogre::Vector3(15, 35, 40));
}

void
Play::destroy_board() {
  State::game_->scene_->remove_child("", "board_root");
  State::game_->scene_->destroy_all_attached_movable_objects
    (State::game_->scene_->get_node("board_root"));
  State::game_->scene_->get_node("board_root")->
    removeAndDestroyAllChildren();
}
