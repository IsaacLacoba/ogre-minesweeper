// -*- coding:utf-8; tab-width:4; mode:cpp -*-
// Minesweeper author: Isaac Lacoba Molina
//   Copyright (C) 2014  ISAAC LACOBA MOLINA
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef STATE_HPP
#define STATE_HPP
#include <memory>

#include "scene.h"
#include "input.h"

class Game;

class State {
  typedef std::string Node;


 protected:
  std::string clicked_button_;

  std::map<Node, std::function<void()>> button_triggers_;

  void add_hook(Node name, std::function<void()> callback);
  std::pair<Ogre::SceneNode*, Ogre::Entity*> create_graphic_element(std::string parent,
                                             std::string node_name, std::string entity_name,
                                             std::string material, Ogre::Vector3 scale,
                                             Ogre::Vector3 position, bool vertical = true,
                                             std::string mesh = "Cell.mesh");
public:
  typedef std::shared_ptr<State> shared;

  std::shared_ptr<Game> game_;

  State(std::shared_ptr<Game> game);
  virtual ~State();

  virtual void init() = 0;
  virtual void exit() = 0;

  void click_button();
  void change_texture();
  std::string get_clicked_node();
};

#endif
