// -*- coding:utf-8; tab-width:4; mode:cpp -*-
// Minesweeper author: Isaac Lacoba Molina
//   Copyright (C) 2014  ISAAC LACOBA MOLINA
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef PLAY_H
#define PLAY_H
#include "state.h"
#include "minesweeper.h"

class Play : public State {
  const float cell_offset = 2;

  Minesweeper minesweeper_;
  bool win_, finish_;
public:
  Play(std::shared_ptr<Game> game);
  virtual ~Play();

  virtual void init();
  virtual void resume();
  virtual void exit();
  virtual void update();


 private:
  void add_callbacks();
  void register_cells_callbacks();
  void move_camera(Minesweeper::Difficulty level);
  void move_camera_easy();
  void move_camera_medium();
  void move_camera_hard();

  void init_board(Minesweeper::Difficulty level);
  void init_ground();
  void init_cells_node_and_entity();
  void destroy_board();

  void reveal_cell();
  void put_flag();
  int get_clicked_button_node_index();
};

#endif
