// -*- coding:utf-8; tab-width:4; mode:cpp -*-
// Minesweeper author: Isaac Lacoba Molina
//   Copyright (C) 2014  ISAAC LACOBA MOLINA
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "game.h"
#include <chrono>
#include <thread>

Menu::Menu(Game::shared game): State::State(game) {
}

void
Menu::init() {
  State::game_->scene_->camera_->setPosition(Ogre::Vector3(0.5 ,2,20));
  State::game_->scene_->camera_->lookAt(Ogre::Vector3(0, 0, 0));

  add_callbacks();
  State::game_->scene_->add_child("", "menu_root");
}

void
Menu::exit() {
  State::game_->input_->clear_hooks();
  State::button_triggers_.clear();
  State::game_->scene_->remove_child("", "menu_root");
  State::game_->set_current_state("play");
}



void
Menu::add_callbacks() {
  State::game_->input_->add_hook({OIS::MB_Left}, std::bind(&State::click_button, this));
  State::add_hook("playbtn_node", std::bind(&Menu::exit, this));
  State::add_hook("creditsbtn_node", std::bind(&Menu::show_credits, this));
  State::add_hook("credits_node", [&]() {
      State::game_->scene_->remove_child("", "credits_node");
      State::game_->scene_->add_child("", "menu_root");
    });
  State::add_hook("exitbtn_node", std::bind(&Menu::shutdown, this));
}


Menu::~Menu() {

}

void
Menu::resume() {

}

void
Menu::update() {

}

void
Menu::show_credits() {
  State::game_->scene_->remove_child("", "menu_root");
  State::game_->scene_->add_child("", "credits_node");
}

void
Menu::shutdown() {
  State::game_->input_->shutdown();
}
