// -*- coding:utf-8; tab-width:4; mode:cpp -*-
// Minesweeper author: Isaac Lacoba Molina
//   Copyright (C) 2014  ISAAC LACOBA MOLINA
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "state.h"
#include "game.h"


State::State(Game::shared game):  game_(game) {

}

State::~State() {

}

std::pair<Ogre::SceneNode*, Ogre::Entity*>
State::create_graphic_element(std::string parent,
                       std::string node_name, std::string entity_name,
                       std::string material, Ogre::Vector3 scale,
                       Ogre::Vector3 position, bool vertical,
                       std::string mesh) {
    Ogre::SceneNode* node = game_->scene_->create_node(node_name);
    node->setScale(scale);
    node->setPosition(position);
    Ogre::Entity* entity = game_->scene_->create_entity(entity_name, mesh);
    if(material != "")
      entity->setMaterialName(material);
    game_->scene_->attach(node, entity);

    game_->scene_->add_child(parent, node_name);

    if(vertical) {
      node->pitch(Ogre::Degree(90), Ogre::SceneNode::TS_LOCAL);
      node->yaw(Ogre::Degree(180), Ogre::SceneNode::TS_LOCAL);
    }

    return std::make_pair(node, entity);
}

void
State::click_button() {
  clicked_button_ = get_clicked_node();
  if(clicked_button_ != "none")
    button_triggers_[clicked_button_]();
}

std::string
State::get_clicked_node() {
  float mouse_x = game_->input_->x;
  float mouse_y = game_->input_->y;
  game_->scene_->set_ray_query(mouse_x, mouse_y);
  Ogre::RaySceneQueryResult &result =
    game_->scene_->ray_query_->execute();

  std::string name;
  for(auto entry: result) {
    name = entry.movable->getParentSceneNode()->getName();
    if(button_triggers_[name]) {
      clicked_button_ = name;
      return name;
    }
  }
  return "none";
}

void
State::add_hook(State::Node name, std::function<void()> callback) {
  if(!button_triggers_[name])
    button_triggers_[name] = callback;
}

void
State::change_texture() {

}
