// -*- coding:utf-8; tab-width:4; mode:cpp -*-
// Minesweeper author: Isaac Lacoba Molina
//   Copyright (C) 2014  ISAAC LACOBA MOLINA

//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.

//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.

//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <iostream>
#include <algorithm>
#include <cmath>
#include <functional>
#include <memory>

#include <bandit/bandit.h>

#include "minesweeper.h"
#include "game.h"


using namespace bandit;

go_bandit([] () {

    describe("minesweeper game", []() {
        static Minesweeper minesweeper;

        before_each( [&](){
              minesweeper = Minesweeper();
              minesweeper.initialize_board(Minesweeper::Difficulty::Easy);
          });

        it("has a board with different size depending on the level difficulty", [&]() {
            Assert::That(minesweeper.board_.size(),
                         Is().EqualTo(Minesweeper::Difficulty::Easy).Or()
                         .EqualTo(Minesweeper::Difficulty::Medium).Or()
                         .EqualTo(Minesweeper::Difficulty::Hard));
          });

        it("can put a flag on a cell in the board", [&]() {
            int cell_index = 0;
            minesweeper.put_flag(cell_index);
            Assert::That(minesweeper.board_[cell_index].is_flagged(),
                         Is().EqualTo(true));
          });

        it("can calculate adyacent cells to a another one", [&](){
            int cell_index = 1;
            Minesweeper::Adyacents adyacent_cells =
              minesweeper.get_adyacent_cells(cell_index);

            Minesweeper::Adyacents real_adyacents_cells;
            cell_index = 0;
            real_adyacents_cells.push_back(cell_index);
            cell_index = 2;
            real_adyacents_cells.push_back(cell_index);
            cell_index = 8;
            real_adyacents_cells.push_back(cell_index);
            cell_index = 9;
            real_adyacents_cells.push_back(cell_index);
            cell_index = 10;
            real_adyacents_cells.push_back(cell_index);


            Assert::That(real_adyacents_cells.size(),
                         Is().EqualTo(adyacent_cells.size()));
          });
      });

    describe("minesweeper game rules", []() {
        static Minesweeper minesweeper;

        before_each( [&](){
            minesweeper = Minesweeper();
            minesweeper.initialize_board(Minesweeper::Difficulty::Easy);
          });

        it("finish the game when active a bomb cell", [&]() {
            int cell_index = 1;
            minesweeper.put_bomb(cell_index);
            minesweeper.active_cell(cell_index);

            Assert::That(minesweeper.is_end(),
                         Is().EqualTo(true));
          });

        it("cannot active a flagged cell", [&]() {
            int cell_index = 0;
            minesweeper.put_flag(cell_index);
            minesweeper.active_cell(cell_index);

            Assert::That(minesweeper.board_[0].is_active(),
                         Is().EqualTo(false));
          });

        it("has a number of bombs equal to the  10 percent of the total cells amount", [&]() {
            int number_of_bombs = std::count_if(minesweeper.board_.begin(),
                                                minesweeper.board_.end(),
                                                std::mem_fn(&Cell::is_a_bomb));
            Assert::That(number_of_bombs,
                         Is().EqualTo((int) (minesweeper.board_.size() * 0.1)));
          });


        it("cells adjacent to a bomb indicate the number of bombs near to them", [&]() {
            Minesweeper::Adyacents proximity_cells;

            for(int mine_index: minesweeper.mines_) {
              proximity_cells = minesweeper.get_adyacent_cells(mine_index);

              Assert::That(minesweeper.board_[mine_index].type_,
                           Is().EqualTo(Cell::Type::Bomb));
              for(int index: proximity_cells){
                Assert::That(minesweeper.board_[index].type_,
                             Is().EqualTo(Cell::Type::Proximity).
                             Or().EqualTo(Cell::Type::Bomb));
              }
            }
          });

        it("active all empty adjacent cells when it active one", [&]() {
            Cell empty_cell;
            for(Cell cell: minesweeper.board_) {
              if(cell.type_ == Cell::Type::Empty){
                empty_cell = cell;
                break;
              }
            }

            int cell_index = minesweeper
              .cartesian_to_index(empty_cell.get_position());
            minesweeper.board_[cell_index].type_ = Cell::Type::Empty;
            minesweeper.board_[cell_index].state_ =  Cell::State::Desactive;
            std::vector<int> actived_cells =
              minesweeper.active_cell(cell_index);

            for(int cell_index: actived_cells) {
                Assert::That(minesweeper.board_[cell_index].is_active(),
                             Is().EqualTo(true));
            }
          });
      });

    describe("game object", [&] () {
        static Game::shared game;
        before_each([&]() {
            game = std::make_shared<Game>();
          });
        it("start the game", [&](){
            game->start();
          });
      });

  });



int main(int argc, char *argv[]) {
  return bandit::run(argc, argv);
}
