// Minesweeper author: Isaac Lacoba Molina
// Copyright (C) 2014  ISAAC LACOBA MOLINA
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#ifndef GAME_HPP
#define GAME_HPP
#include <memory>

#include "initial.h"
#include "menu.h"
#include "play.h"


class Game: public std::enable_shared_from_this<Game> {
  typedef std::map<std::string, State::shared> StateTable;

  StateTable state_table_;

 public:
  typedef std::shared_ptr<Game> shared;


  Scene::shared scene_;
  EventListener::shared input_;

  Game();
  virtual ~Game();

  void start();

  void set_current_state(std::string next_state);

 private:
  void game_loop();
};

#endif
