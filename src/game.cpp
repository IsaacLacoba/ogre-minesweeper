// -*- coding:utf-8; tab-width:4; mode:cpp -*-
// Minesweeper author: Isaac Lacoba Molina
// Copyright (C) 2014  ISAAC LACOBA MOLINA
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "game.h"

Game::Game() {
  scene_ = std::make_shared<Scene>();
  input_ = std::make_shared<EventListener>(scene_->window_);

}

Game::~Game() {
}

void
Game::start() {
  state_table_["initial"] = std::make_shared<Initial>(shared_from_this());
  state_table_["menu"] = std::make_shared<Menu>(shared_from_this());
  state_table_["play"] = std::make_shared<Play>(shared_from_this());

  set_current_state("initial");
  game_loop();
}

void
Game::set_current_state(std::string next_state) {
  state_table_[next_state]->init();
}

void
Game::game_loop() {
  while(!input_->exit_) {
    input_->capture();
    input_->check_events();
    scene_->render_one_frame();
  }
}
