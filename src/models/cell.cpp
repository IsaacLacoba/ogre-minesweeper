// -*- coding:utf-8; tab-width:4; mode:cpp -*-
// Minesweeper author: Isaac Lacoba Molina
//   Copyright (C) 2014  ISAAC LACOBA MOLINA

//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.

//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.

//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "cell.h"

Cell::Cell() {
  type_ = Cell::Type::Empty;
  state_ = Cell::State::Desactive;
  adyacent_mines_ = 0;
  node_ = 0;
  entity_ = 0;
  flag_ = 0;
  flagged_ = false;
}

void
Cell::initialize(Cell::Type type, Cell::Position position) {
  position_ = position;
  type_ = type;
}


void
Cell::initialize(Ogre::SceneNode* node, Ogre::Entity* entity) {
  entity_ = entity;
  node_ = node;
  node_->yaw(Ogre::Degree(180));
  flag_ = static_cast<Ogre::SceneNode*>(node_->getChild(0));
  static_cast<Ogre::Entity*>(flag_->getAttachedObject(0))->setCastShadows(true);
  flag_->pitch(Ogre::Degree(180));

  flag_->setVisible(false);

}

Cell::Position
Cell::get_position() {
  return position_;
}

bool
Cell::is_active() {
  return state_ == Cell::State::Active;
}

bool
Cell::is_flagged() {
  return flagged_;
}

bool
Cell::is_a_bomb() {
  return type_ == Cell::Type::Bomb;
}

void
Cell::set_proximity() {
  if(!Cell::is_a_bomb())
    type_= Cell::Type::Proximity;
  adyacent_mines_++;
}

void
Cell::put_flag() {
  if(state_ == Cell::State::Active)
    return;

  flagged_ = !flagged_;
  if(node_)
    static_cast<Ogre::SceneNode*>(node_->getChild(0))->setVisible(flagged_);
}

void
Cell::put_bomb() {
  type_ = Cell::Type::Bomb;
}

void
Cell::active_cell() {
  if(flagged_)
    return;

  std::string material;
  if(type_ == Cell::Type::Bomb)
    material = "mine";
  if(type_ == Cell::Type::Proximity)
    material = std::to_string(adyacent_mines_);
  if(type_ == Cell::Type::Empty)
    material = "empty";

  state_ = Cell::State::Active;
  if(entity_)
    entity_->setMaterialName(material);
}

std::string
Cell::to_string() {
std::stringstream description;
 description << " type: " << type_
             << " state: " << state_
             << " proximity: " << adyacent_mines_;
 return description.str();
}
