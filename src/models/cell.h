// Minesweeper author: Isaac Lacoba Molina
//   Copyright (C) 2014  ISAAC LACOBA MOLINA

//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.

//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.

//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef CELL_H
#define CELL_H
#include <iostream>
#include <vector>
#include <cmath>
#include <sstream>
#include <string>

#include <OgreSceneNode.h>
#include <OgreEntity.h>


class Cell {
 public:
  enum Type{Empty = 0, Bomb = 1, Proximity = 2};
  enum State{Active = 3, Desactive = 4};
  typedef std::pair<int, int> Position;
  typedef int Counter;

  Type type_;
  Counter adyacent_mines_;
  State state_;
  bool flagged_;

  Ogre::SceneNode* node_;
  Ogre::Entity* entity_;
  Ogre::SceneNode* flag_;

  Cell();
  void initialize(Type type, Position position);
  void initialize(Ogre::SceneNode* node, Ogre::Entity* entity);

  void set_type(Type type);
  void put_flag();
  void put_bomb();
  void active_cell();
  void set_proximity();

  bool is_active();
  bool is_flagged();
  bool is_a_bomb();

  std::string to_string();

  Position get_position();

 private:
  Position position_;

};

#endif
