// Minesweeper author: Isaac Lacoba Molina
//   Copyright (C) 2014  ISAAC LACOBA MOLINA

//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.

//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.

//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef GAME_H
#define GAME_H
#include <vector>
#include <algorithm>
#include <cmath>
#include <set>

#include "cell.h"

class Minesweeper {
  int flags_;

 public:
  typedef std::vector<Cell> Board;
  typedef std::vector<int> Mines;
  typedef std::vector<int> Adyacents;
  enum Difficulty {Easy=8*8, Medium=16*16, Hard=16*30 + 3};

  Difficulty level_;
  Board board_;
  Mines mines_;

  void initialize_board(Difficulty difficulty);

  std::vector<int> active_cell(int cell_index);
  void put_flag(int cell_index);
  void put_bomb(int cell_index);
  bool is_end();
  bool is_game_win();
  bool is_game_lose();

  Adyacents get_adyacent_cells(int cell_index);

  int get_node_index(Ogre::SceneNode* node, int cell_offset);
  Cell::Position index_to_cartesian(int index);
  int cartesian_to_index(Cell::Position);



 private:
  int columns_, rows_;

  std::vector<int> active_adjacent_cells(int cell_index);
  void update_proximity_cells(int random_position);

  std::set<int> get_activated_adjacent_cells(int cell_index);
};
#endif
