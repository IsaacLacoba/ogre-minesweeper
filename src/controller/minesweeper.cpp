// -*- coding:utf-8; tab-width:4; mode:cpp -*-
// Minesweeper author: Isaac Lacoba Molina
//   Copyright (C) 2014  ISAAC LACOBA MOLINA

//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.

//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.

//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "minesweeper.h"

void
Minesweeper::initialize_board(Minesweeper::Difficulty level) {
  board_ = std::vector<Cell>(level);
  rows_ = columns_ = std::sqrt(board_.size());
  int index = -1;
  int number_of_bombs = (int) level * 0.1;
  flags_ = number_of_bombs;

  std::random_device random_device;
  std::mt19937 generator(random_device());
  std::uniform_int_distribution<> random(1, board_.size());

  for(Cell& cell: board_)
    cell.initialize(Cell::Type::Empty, index_to_cartesian(++index));


  while(0 != number_of_bombs){
    int random_position = random(generator) - 1;
    if(board_[random_position].is_a_bomb())
      continue;
    put_bomb(random_position);
    number_of_bombs--;
  }
}

std::vector<int>
Minesweeper::active_cell(int cell_index) {
  if(board_[cell_index].is_flagged())
    return std::vector<int>();

  board_[cell_index].active_cell();

  if(board_[cell_index].is_a_bomb())
    for(int mine: mines_){
      board_[mine].active_cell();
    }

  if(board_[cell_index].type_ == Cell::Type::Empty)
    return active_adjacent_cells(cell_index);
  return std::vector<int>();
}


std::vector<int>
Minesweeper::active_adjacent_cells(int cell_index) {
  std::set<int> temporal, adjacent_cells;
  std::vector<int> cells_activated;

  adjacent_cells = get_activated_adjacent_cells(cell_index);

  while(adjacent_cells.size() != 0) {
    cells_activated.assign(adjacent_cells.begin(),
                           adjacent_cells.end());
    adjacent_cells.clear();
    for(int cell: cells_activated) {
      if(board_[cell].type_ == Cell::Type::Empty) {
        temporal = get_activated_adjacent_cells(cell);
        adjacent_cells.insert(temporal.begin(),
                              temporal.end());
      }
    }
  }
  return cells_activated;
}

std::set<int>
Minesweeper::get_activated_adjacent_cells(int cell_index) {
  std::set<int> cells_desactivated;

  for(int adjacent_cell: get_adyacent_cells(cell_index)){
    if(!board_[adjacent_cell].is_active()) {
      board_[adjacent_cell].active_cell();
      cells_desactivated.insert(adjacent_cell);
    }
  }

  return cells_desactivated;
}

void
Minesweeper::put_flag(int cell_index) {
  if(board_[cell_index].is_flagged()) {
    flags_++;
    board_[cell_index].put_flag();
    return;
  }

  if(flags_ == 0)
    return;

  board_[cell_index].put_flag();
  flags_--;
}

void
Minesweeper::put_bomb(int cell_index) {
  board_[cell_index].put_bomb();
  mines_.push_back(cell_index);
  update_proximity_cells(cell_index);

}

bool
Minesweeper::is_end() {
  return is_game_lose() || is_game_win();
}

bool
Minesweeper::is_game_win() {
  int active_cells = std::count_if
    (board_.begin(), board_.end(),std::mem_fn(&Cell::is_active));

  return (unsigned int) active_cells == (board_.size() - mines_.size());
}

bool
Minesweeper::is_game_lose() {
    int number_of_active_bombs = std::count_if(mines_.begin(), mines_.end(), [&](int cell) {
        return board_[cell].is_active();
      });

    return number_of_active_bombs > 0;
}

int
Minesweeper::get_node_index(Ogre::SceneNode* node, int cell_offset) {

  float x = node->getPosition().x / cell_offset;
  float z = node->getPosition().z / cell_offset;

  return cartesian_to_index(Cell::Position(x, z));
}

Cell::Position
Minesweeper::index_to_cartesian(int index) {
  int x = (index % rows_);
  int y = index / columns_;
  return Cell::Position(x, y);
}

int
Minesweeper::cartesian_to_index(Cell::Position position) {
  return position.first + (position.second * columns_);
}

Minesweeper::Adyacents
Minesweeper::get_adyacent_cells(int cell_index) {
  Adyacents adyacents;
  for(int i = -1; i <= 1; ++i) {
    for(int j = -1; j <= 1; ++j) {
      int index = (cell_index + j) + (rows_ * i);
      if (index == cell_index)
        continue;
      if(((cell_index % 8 == 0) && (index % 8 == 7))
         || ((cell_index % 8 == 7) && (index % 8 == 0)))
        continue;
      if(((unsigned int)index < board_.size()) && index >= 0)
        adyacents.push_back(index);
      }
    }

  return adyacents;
}

void
Minesweeper::update_proximity_cells(int cell_index) {
  Minesweeper::Adyacents index_adjacents =
    get_adyacent_cells(cell_index);
  for(int index: index_adjacents) {
    board_[index].set_proximity();
  }
}
