![minesweeper.jpg](https://bitbucket.org/repo/kGXkyn/images/1669524078-minesweeper.jpg)

# Minesweeper #
This is first delivery from Expert Development Videogame grade taught in the High School of Computer Science, Ciudad Real.

Link to video: https://www.youtube.com/watch?v=FnfHswcwqq4&list=UUEi3kQBN80K2FV0MpIbNLdQ

* **LICENSE**:

**Minesweeper author: Isaac Lacoba Molina
   Copyright (C) 2014  ISAAC LACOBA MOLINA**

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     (at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.